### Задание 1. Создать Deployment и обеспечить доступ к репликам приложения из другого Pod

1. Создать Deployment приложения, состоящего из двух контейнеров — nginx и multitool. Решить возникшую ошибку.

Ошибки не было в связи с указанием другого порта на мультитуле.

```
kubectl apply -f nginx+multitool.yaml
deployment.apps/multitool created

kubectl get pod                      
NAME                         READY   STATUS              RESTARTS   AGE
multitool-67fbc6b755-vhgmc   0/2     ContainerCreating   0          12s

kubectl get po
NAME                         READY   STATUS    RESTARTS   AGE
multitool-67fbc6b755-vhgmc   2/2     Running   0          8m41s
```

2. После запуска увеличить количество реплик работающего приложения до 2.

Скейл деплоймента через консоль, но лучше изменения применять через конфиг.

```
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl scale --replicas=2 deployment multitool 
deployment.apps/multitool scaled
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl get po                                 
NAME                         READY   STATUS    RESTARTS   AGE
multitool-67fbc6b755-jvksw   2/2     Running   0          8s
multitool-67fbc6b755-vhgmc   2/2     Running   0          55m
```

3. Продемонстрировать количество подов до и после масштабирования.

До:

```
kubectl get po
NAME                         READY   STATUS    RESTARTS   AGE
multitool-67fbc6b755-vhgmc   2/2     Running   0          8m41s
```

После:

```
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl get po                                 
NAME                         READY   STATUS    RESTARTS   AGE
multitool-67fbc6b755-jvksw   2/2     Running   0          8s
multitool-67fbc6b755-vhgmc   2/2     Running   0          55m
```

4. Создать Service, который обеспечит доступ до реплик приложений из п.1.

```
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl apply -f multitool_svc.yaml    
service/multitool-svc configured
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl get svc                        
NAME            TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)           AGE
multitool-svc   ClusterIP   10.152.183.41   <none>        81/TCP,8081/TCP   116s
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl describe services multitool-svc
Name:              multitool-svc
Namespace:         netology-test
Labels:            <none>
Annotations:       <none>
Selector:          app=multitoolapp
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.152.183.41
IPs:               10.152.183.41
Port:              web-nginx  81/TCP
TargetPort:        80/TCP
Endpoints:         10.1.243.254:80,10.1.243.255:80
Port:              web-multitool  8081/TCP
TargetPort:        8080/TCP
Endpoints:         10.1.243.254:8080,10.1.243.255:8080
Session Affinity:  None
Events:            <none>
```

5. Создать отдельный Pod с приложением multitool и убедиться с помощью `curl`, что из пода есть доступ до приложений из п.1.

Доступ до nginx'a есть, в сервисе также прописал тот кастомный порт, чтобы не ругался куб при аплае. 
Только по окончанию ДЗ увидел прикрепленный мультитул, где прописаны порты)) Использовал левый образ, но принципе понятен.

```
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl apply -f solomultitool.yaml 
pod/multitool created
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl get pod
NAME                         READY   STATUS    RESTARTS   AGE
multitool                    1/1     Running   0          6s
multitool-67fbc6b755-jvksw   2/2     Running   0          12m
multitool-67fbc6b755-vhgmc   2/2     Running   0          68m
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl exec multitool -- curl http://10.152.183.41:81
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
100   612  100   612    0     0  30464      0 --:--:-- --:--:-- --:--:-- 32210
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl exec multitool -- curl http://10.152.183.41:8081
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
curl: (7) Failed to connect to 10.152.183.41 port 8081 after 0 ms: Connection refused
command terminated with exit code 7
```

### Задание 2. Создать Deployment и обеспечить старт основного контейнера при выполнении условий

1. Создать Deployment приложения nginx и обеспечить старт контейнера только после того, как будет запущен сервис этого приложения.

[nginx_init.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/1.3/nginx+init.yaml?ref_type=heads)

2. Убедиться, что nginx не стартует. В качестве Init-контейнера взять busybox.

```
└> kubectl get pods nginx-init-65cc6cc8b5-7q9tr 
NAME                          READY   STATUS     RESTARTS   AGE
nginx-init-65cc6cc8b5-7q9tr   0/1     Init:0/1   0          8s
└> kubectl logs nginx-init-65cc6cc8b5-7q9tr -c init-svc
Server:    10.152.183.10
Address 1: 10.152.183.10 kube-dns.kube-system.svc.cluster.local

nslookup: can't resolve 'nginx-svc.netology-test.svc.cluster.local'
waiting for nginx-svc
```

3. Создать и запустить Service. Убедиться, что Init запустился.

[nginx_svc.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/1.3/nginx_svc.yaml?ref_type=heads)

```
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl apply -f nginx_svc.yaml 
service/nginx-svc created
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl logs nginx-init-65cc6cc8b5-7q9tr -c init-svc
Server:    10.152.183.10
Address 1: 10.152.183.10 kube-dns.kube-system.svc.cluster.local
nslookup: can't resolve 'nginx-svc.netology-test.svc.cluster.local'
waiting for nginx-svc
***
Server:    10.152.183.10
Address 1: 10.152.183.10 kube-dns.kube-system.svc.cluster.local

Name:      nginx-svc.netology-test.svc.cluster.local
Address 1: 10.152.183.34 nginx-svc.netology-test.svc.cluster.local
```
4. Продемонстрировать состояние пода до и после запуска сервиса.

```
└> kubectl get pods nginx-init-65cc6cc8b5-7q9tr 
NAME                          READY   STATUS    RESTARTS   AGE
nginx-init-65cc6cc8b5-7q9tr   1/1     Running   0          4m35s
┌[ubuntu☮ubuntu]-(~/netology/repos/kuber/1.3)-[git://1.3 ✗]-
└> kubectl describe pods nginx-init-65cc6cc8b5-7q9tr 
***
Init Containers:
  init-svc:
    Container ID:  containerd://3635e2b372118d56afaa218ba9fca1b8ebfc7b298f06137934de617a8a2f531f
    Image:         busybox:1.28
    Image ID:      docker.io/library/busybox@sha256:141c253bc4c3fd0a201d32dc1f493bcf3fff003b6df416dea4f41046e0f37d47
    Port:          <none>
    Host Port:     <none>
    Command:
      sh
      -c
      until nslookup nginx-svc.$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace).svc.cluster.local; do echo waiting for nginx-svc; sleep 2; done
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Thu, 16 May 2024 13:21:38 +0300
      Finished:     Thu, 16 May 2024 13:23:32 +0300
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-pc2nk (ro)
Containers:
  nginx:
    Container ID:   containerd://fcf37f5fb78bb585d41c98e1b77c0503c6a34e2a93b92f7c5a2654fe63ae2e21
    Image:          nginx:1.16.1
    Image ID:       docker.io/library/nginx@sha256:d20aa6d1cae56fd17cd458f4807e0de462caf2336f0b70b5eeb69fcaaf30dd9c
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Thu, 16 May 2024 13:23:34 +0300
    Ready:          True
    Restart Count:  0
    Environment:    <none>
```
