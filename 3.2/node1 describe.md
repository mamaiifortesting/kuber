```
Name:               node1
Roles:              control-plane
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=node1
                    kubernetes.io/os=linux
                    node-role.kubernetes.io/control-plane=
                    node.kubernetes.io/exclude-from-external-load-balancers=
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: unix:///var/run/containerd/containerd.sock
                    node.alpha.kubernetes.io/ttl: 0
                    projectcalico.org/IPv4Address: 10.0.1.21/24
                    projectcalico.org/IPv4VXLANTunnelAddr: 10.233.102.128
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Fri, 28 Jun 2024 14:34:36 +0000
Taints:             node-role.kubernetes.io/control-plane:NoSchedule
Unschedulable:      false
Lease:
  HolderIdentity:  node1
  AcquireTime:     <unset>
  RenewTime:       Fri, 28 Jun 2024 15:38:55 +0000
Conditions:
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----                 ------  -----------------                 ------------------                ------                       -------
  NetworkUnavailable   False   Fri, 28 Jun 2024 14:36:55 +0000   Fri, 28 Jun 2024 14:36:55 +0000   CalicoIsUp                   Calico is running on this node
  MemoryPressure       False   Fri, 28 Jun 2024 15:38:55 +0000   Fri, 28 Jun 2024 14:34:36 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure         False   Fri, 28 Jun 2024 15:38:55 +0000   Fri, 28 Jun 2024 14:34:36 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure          False   Fri, 28 Jun 2024 15:38:55 +0000   Fri, 28 Jun 2024 14:34:36 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready                True    Fri, 28 Jun 2024 15:38:55 +0000   Fri, 28 Jun 2024 14:38:05 +0000   KubeletReady                 kubelet is posting ready status. AppArmor enabled
Addresses:
  InternalIP:  10.0.1.21
  Hostname:    node1
Capacity:
  cpu:                2
  ephemeral-storage:  51505464Ki
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             2018600Ki
  pods:               110
Allocatable:
  cpu:                2
  ephemeral-storage:  47467435544
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             1916200Ki
  pods:               110
System Info:
  Machine ID:                 23000007c6c69d05a0b03fd6d80b55bc
  System UUID:                23000007-c6c6-9d05-a0b0-3fd6d80b55bc
  Boot ID:                    fc6f430c-6661-4d35-a764-fea8ed73b6e9
  Kernel Version:             5.4.0-186-generic
  OS Image:                   Ubuntu 20.04.6 LTS
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  containerd://1.7.16
  Kubelet Version:            v1.29.5
  Kube-Proxy Version:         v1.29.5
Non-terminated Pods:          (8 in total)
  Namespace                   Name                               CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age
  ---------                   ----                               ------------  ----------  ---------------  -------------  ---
  kube-system                 calico-node-hrtxn                  150m (7%)     300m (15%)  64M (3%)         500M (25%)     63m
  kube-system                 coredns-69db55dd76-xhk74           100m (5%)     0 (0%)      70Mi (3%)        300Mi (16%)    61m
  kube-system                 dns-autoscaler-6f4b597d8c-w8q2t    20m (1%)      0 (0%)      10Mi (0%)        0 (0%)         61m
  kube-system                 kube-apiserver-node1               250m (12%)    0 (0%)      0 (0%)           0 (0%)         64m
  kube-system                 kube-controller-manager-node1      200m (10%)    0 (0%)      0 (0%)           0 (0%)         64m
  kube-system                 kube-proxy-gnd6g                   0 (0%)        0 (0%)      0 (0%)           0 (0%)         63m
  kube-system                 kube-scheduler-node1               100m (5%)     0 (0%)      0 (0%)           0 (0%)         64m
  kube-system                 nodelocaldns-4fs8v                 100m (5%)     0 (0%)      70Mi (3%)        200Mi (10%)    61m
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests         Limits
  --------           --------         ------
  cpu                920m (46%)       300m (15%)
  memory             221286400 (11%)  1024288k (52%)
  ephemeral-storage  0 (0%)           0 (0%)
  hugepages-1Gi      0 (0%)           0 (0%)
  hugepages-2Mi      0 (0%)           0 (0%)
Events:
  Type     Reason                   Age   From             Message
  ----     ------                   ----  ----             -------
  Normal   Starting                 60m   kubelet          Starting kubelet.
  Warning  InvalidDiskCapacity      60m   kubelet          invalid capacity 0 on image filesystem
  Normal   NodeHasSufficientMemory  60m   kubelet          Node node1 status is now: NodeHasSufficientMemory
  Normal   NodeHasNoDiskPressure    60m   kubelet          Node node1 status is now: NodeHasNoDiskPressure
  Normal   NodeHasSufficientPID     60m   kubelet          Node node1 status is now: NodeHasSufficientPID
  Normal   NodeNotReady             60m   kubelet          Node node1 status is now: NodeNotReady
  Normal   NodeAllocatableEnforced  60m   kubelet          Updated Node Allocatable limit across pods
  Normal   NodeReady                60m   kubelet          Node node1 status is now: NodeReady
  Normal   RegisteredNode           60m   node-controller  Node node1 event: Registered Node node1 in Controller
```