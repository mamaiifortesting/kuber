### Задание 1. Установить кластер k8s с 1 master node

1. Подготовка работы кластера из 5 нод: 1 мастер и 4 рабочие ноды.
![alt text](Screenshot_18.png)

2. В качестве CRI — containerd.
3. Запуск etcd производить на мастере.
4. Способ установки выбрать самостоятельно.
- Выбрал kubespray, всё прошло гладко.
![alt text](Screenshot_16.png)

- Дескрайбы [мастера](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/3.2/node1%20describe.md?ref_type=heads) и [воркер ноды](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/3.2/node2%20describe.md?ref_type=heads)

![alt text](Screenshot_17.png)
 