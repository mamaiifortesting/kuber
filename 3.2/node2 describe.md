```
Name:               node2
Roles:              <none>
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=node2
                    kubernetes.io/os=linux
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: unix:///var/run/containerd/containerd.sock
                    node.alpha.kubernetes.io/ttl: 0
                    projectcalico.org/IPv4Address: 10.0.1.11/24
                    projectcalico.org/IPv4VXLANTunnelAddr: 10.233.75.0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Fri, 28 Jun 2024 14:35:11 +0000
Taints:             <none>
Unschedulable:      false
Lease:
  HolderIdentity:  node2
  AcquireTime:     <unset>
  RenewTime:       Fri, 28 Jun 2024 15:38:57 +0000
Conditions:
  Type                 Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----                 ------  -----------------                 ------------------                ------                       -------
  NetworkUnavailable   False   Fri, 28 Jun 2024 14:36:27 +0000   Fri, 28 Jun 2024 14:36:27 +0000   CalicoIsUp                   Calico is running on this node
  MemoryPressure       False   Fri, 28 Jun 2024 15:38:54 +0000   Fri, 28 Jun 2024 14:35:11 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure         False   Fri, 28 Jun 2024 15:38:54 +0000   Fri, 28 Jun 2024 14:35:11 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure          False   Fri, 28 Jun 2024 15:38:54 +0000   Fri, 28 Jun 2024 14:35:11 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready                True    Fri, 28 Jun 2024 15:38:54 +0000   Fri, 28 Jun 2024 14:36:47 +0000   KubeletReady                 kubelet is posting ready status. AppArmor enabled
Addresses:
  InternalIP:  10.0.1.11
  Hostname:    node2
Capacity:
  cpu:                2
  ephemeral-storage:  51505464Ki
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             2018600Ki
  pods:               110
Allocatable:
  cpu:                2
  ephemeral-storage:  47467435544
  hugepages-1Gi:      0
  hugepages-2Mi:      0
  memory:             1916200Ki
  pods:               110
System Info:
  Machine ID:                 23000007c6de1bb8d19c408a3bafb47d
  System UUID:                23000007-c6de-1bb8-d19c-408a3bafb47d
  Boot ID:                    c5ec9322-8abd-40f3-ac54-6199a07790ae
  Kernel Version:             5.4.0-186-generic
  OS Image:                   Ubuntu 20.04.6 LTS
  Operating System:           linux
  Architecture:               amd64
  Container Runtime Version:  containerd://1.7.16
  Kubelet Version:            v1.29.5
  Kube-Proxy Version:         v1.29.5
Non-terminated Pods:          (4 in total)
  Namespace                   Name                  CPU Requests  CPU Limits  Memory Requests  Memory Limits  Age
  ---------                   ----                  ------------  ----------  ---------------  -------------  ---
  kube-system                 calico-node-v5wk6     150m (7%)     300m (15%)  64M (3%)         500M (25%)     63m
  kube-system                 kube-proxy-s5p5n      0 (0%)        0 (0%)      0 (0%)           0 (0%)         63m
  kube-system                 nginx-proxy-node2     25m (1%)      0 (0%)      32M (1%)         0 (0%)         63m
  kube-system                 nodelocaldns-dcfsz    100m (5%)     0 (0%)      70Mi (3%)        200Mi (10%)    61m
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests        Limits
  --------           --------        ------
  cpu                275m (13%)      300m (15%)
  memory             169400320 (8%)  709715200 (36%)
  ephemeral-storage  0 (0%)          0 (0%)
  hugepages-1Gi      0 (0%)          0 (0%)
  hugepages-2Mi      0 (0%)          0 (0%)
Events:
  Type    Reason          Age   From             Message
  ----    ------          ----  ----             -------
  Normal  RegisteredNode  60m   node-controller  Node node2 event: Registered Node node2 in Controller
  ```