### Задание 1

**Что нужно сделать**

Создать Deployment приложения, использующего локальный PV, созданный вручную.

1. Создать Deployment приложения, состоящего из контейнеров busybox и multitool.
- [app-deploy.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/2.2/app-deploy.yaml)
2. Создать PV и PVC для подключения папки на локальной ноде, которая будет использована в поде.
- [pv.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/2.2/pv.yaml), [pvc.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/2.2/pvc.yaml?ref_type=heads)

![alt text](Screenshot_50.png)

3. Продемонстрировать, что multitool может читать файл, в который busybox пишет каждые пять секунд в общей директории. 

![alt text](Screenshot_49.png)

4. Удалить Deployment и PVC. Продемонстрировать, что после этого произошло с PV. Пояснить, почему.
- Волюм сохраняется без возможности подключить его к другим ресурсам. В ручном режиме необходимо решить его дальнейшую судьбу.

![alt text](Screenshot_51.png)

5. Продемонстрировать, что файл сохранился на локальном диске ноды. Удалить PV.  Продемонстрировать что произошло с файлом после удаления PV. Пояснить, почему.
- Файл остался после удаления PV из-за политики по умолчанию Retain, которая применима к PV. Retain - после удаление PVC сохраняет данные PV локально.

![alt text](Screenshot_85.png) 

5. Предоставить манифесты, а также скриншоты или вывод необходимых команд.

------

### Задание 2

**Что нужно сделать**

Создать Deployment приложения, которое может хранить файлы на NFS с динамическим созданием PV.

1. Включить и настроить NFS-сервер на MicroK8S.
2. Создать Deployment приложения состоящего из multitool, и подключить к нему PV, созданный автоматически на сервере NFS.

- [nfs-deploy.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/2.2/nfs-deploy.yaml?ref_type=heads)

![alt text](Screenshot_87.png)

3. Продемонстрировать возможность чтения и записи файла изнутри пода. 

![alt text](Screenshot_89.png) ![alt text](Screenshot_88.png)

4. Предоставить манифесты, а также скриншоты или вывод необходимых команд.

------