### Задание 1. Создать Deployment приложений backend и frontend

1. Создать Deployment приложения _frontend_ из образа nginx с количеством реплик 3 шт.
2. Создать Deployment приложения _backend_ из образа multitool. 
3. Добавить Service, которые обеспечат доступ к обоим приложениям внутри кластера. 
4. Продемонстрировать, что приложения видят друг друга с помощью Service.
5. Предоставить манифесты Deployment и Service в решении, а также скриншоты или вывод команды п.4.

![alt text](Screenshot_44.jpg)

------

### Задание 2. Создать Ingress и обеспечить доступ к приложениям снаружи кластера

1. Включить Ingress-controller в MicroK8S.

![alt text](image.png)

2. Создать Ingress, обеспечивающий доступ снаружи по IP-адресу кластера MicroK8S так, чтобы при запросе только по адресу открывался _frontend_ а при добавлении /api - _backend_.

[ingress.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/1.5/ingress-rule.yaml)

3. Продемонстрировать доступ с помощью браузера или `curl` с локального компьютера.

![alt text](image-2.png)

4. Предоставить манифесты и скриншоты или вывод команды п.2.
