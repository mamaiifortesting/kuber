### Задание 1. Создайте конфигурацию для подключения пользователя

1. Создайте и подпишите SSL-сертификат для подключения к кластеру.

![alt text](Screenshot_6.png)

2. Настройте конфигурационный файл kubectl для подключения.

![alt text](image.png)

3. Создайте роли и все необходимые настройки для пользователя.
- [role.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/2.4/role.yaml?ref_type=heads)/[rolebinding.yaml](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/2.4/rolebinding.yaml?ref_type=heads)

4. Предусмотрите права пользователя. Пользователь может просматривать логи подов и их конфигурацию (`kubectl logs pod <pod_id>`, `kubectl describe pod <pod_id>`).

![alt text](image-1.png)

5. Предоставьте манифесты и скриншоты и/или вывод необходимых команд.

------

# Попытка номер два

![alt text](Screenshot_8.png)