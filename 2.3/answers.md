### Задание 1. Создать Deployment приложения и решить возникшую проблему с помощью ConfigMap. Добавить веб-страницу

1. Создать Deployment приложения, состоящего из контейнеров nginx и multitool. 

- [deployment](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/2.3/nginx-multitool-deployment.yaml?ref_type=heads)

2. Решить возникшую проблему с помощью ConfigMap.
3. Продемонстрировать, что pod стартовал и оба конейнера работают.
![alt text](Screenshot_1.png)

4. Сделать простую веб-страницу и подключить её к Nginx с помощью ConfigMap. Подключить Service и показать вывод curl или в браузере.
![alt text](Screenshot_2.png)

5. Предоставить манифесты, а также скриншоты или вывод необходимых команд.
------

### Задание 2. Создать приложение с вашей веб-страницей, доступной по HTTPS 

1. Создать Deployment приложения, состоящего из Nginx.

- [deployment](https://gitlab.com/mamaiifortesting/kuber/-/blob/main/2.3/nginx-deploy.yaml?ref_type=heads)

2. Создать собственную веб-страницу и подключить её как ConfigMap к приложению.
![alt text](image.png)

3. Выпустить самоподписной сертификат SSL. Создать Secret для использования сертификата.
![alt text](image-1.png)
4. Создать Ingress и необходимый Service, подключить к нему SSL в вид. Продемонстировать доступ к приложению по HTTPS. 
![alt text](Screenshot_3.png)
![alt text](Screenshot_4.png)

4. Предоставить манифесты, а также скриншоты или вывод необходимых команд.

------